window.onload = ()=>{

  // JavaScript is (almost) like the commander
  
  // all the div in the DOM 
  
  let divOne = document.querySelector('.divBlue-1')
  
  let divTwo = document.querySelector('.divBlue-2')
  
  let divThree = document.querySelector('.divBlue-3')
  
  let divFour = document.querySelector('.divBlue-4')

  // all the text in the DOM 

  let textOne= document.querySelector('.text-1')

  let textTwo = document.querySelector('.text-2')

  let textThree = document.querySelector('.text-3')

  let textFour = document.querySelector('.text-4')  
  
  // all the functions that set a delay 
  
  // you can set a different time out to each other
  
  function setTimeout1(){
 
       setTimeout(()=>{
          
          textOne.classList.remove('text-3')
                
          textOne.classList.add('textChart')
       
        }, 550
       
      )
        
    }

  function setTimeout2(){
 
      setTimeout(()=>{
         
         textTwo.classList.remove('text-3')
               
         textTwo.classList.add('textChart')
       
        }, 550
      
      )
    
    }

  function setTimeout3(){
 
    setTimeout(()=>{
       
       textThree.classList.remove('text-3')
             
       textThree.classList.add('textChart')
     
      }, 550
    
    )
     
   }

  function setTimeout4(){
 
    setTimeout(()=>{
     
     textFour.classList.remove('text-4')
           
     textFour.classList.add('textChart')
     
     }, 550
  
    )
   
   }

// a loop   

for(let i = 0; i < 4; i++){

// an event on scroll

  window.addEventListener("scroll", (e)=>{

      let scroll = window.scrollY
      
      // console.log(window.pageYOffset)
      
      // All yours conditions that check the window to the offset Y axis value on the page
      
      // if it succeed that modify the class' name
      
      if(scroll > 100){ 
        
        divOne.classList.remove('divBlue-1')
        
        divOne.classList.add('divDarkBlue')

        setTimeout1() 

      }

      if(scroll > 300){
             
        divTwo.classList.remove('divBlue-2')    
        
        divTwo.classList.add('divDarkBlue')

        setTimeout2()
             
      }

      if(scroll > 700){
                
        divThree.classList.remove('divBlue-3')        
        
        divThree.classList.add('divDarkBlue')

        setTimeout3()
                 
      }
           
      if(scroll > 1000){
                   
        divFour.classList.remove('divBlue-4')         
        
        divFour.classList.add('divDarkBlue')

        setTimeout4()    
                  
      }
                
  })

}

}


















